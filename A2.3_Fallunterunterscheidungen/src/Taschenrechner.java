import java.util.Scanner;
public class Taschenrechner {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Taschenrechner");
		System.out.println("Bitte geben Sie die erste der beiden Zahlen ein");
		float num1 = myScanner.nextFloat();
		System.out.println("Bitte geben Sie die zweite der beiden Zahlen ein");
		float num2 = myScanner.nextFloat();
		System.out.println("Sollen diese Zahlen addiert (+), subtrahier (-), multipliziert (*) oder dividiert (/) werden?");
		char math = myScanner.next().charAt(0);
		while (math != '+' && math != '-' && math != '*' && math != '/') {
			System.out.println("Hier scheint ein Fehler passiert zu sein");
			System.out.println("Sollen diese Zahlen addiert (+), subtrahier (-), multipliziert (*) oder dividiert (/) werden?");
			math = myScanner.next().charAt(0);
		}
		float result;
		myScanner.close();
		if (math == '+') {
			result = num1 + num2;
			System.out.printf("Das Ergebnis der Addition von %.2f und %.2f ist %.2f", num1, num2, result);
		}
		if (math == '-') {
			result = num1 - num2;
			System.out.printf("Das Ergebnis der Subtraktion von %.2ff und %.2f ist %.2f", num1, num2, result);
		}
		if (math == '*') {
			result = num1 * num2;
			System.out.printf("Das Ergebnis der Multiplikation von %.2f und %.2f ist %.2f", num1, num2, result);
		}
		if (math == '/') {
			result = num1 / num2;
			System.out.printf("Das Ergebnis der Division von %.2f und %.2f ist %.2f", num1, num2, result);
		}
	}
}