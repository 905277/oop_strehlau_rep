
public class Temperaturtabelle {

	public static void main(String[] args) {
		int f1 = -20;
		int f2 = -10;
		int f3 = 0;
		int f4 = 20;
		int f5 = 30;
		float c1 = -28.8889f;
		float c2 = -23.3333f;
		float c3 = -17.7778f;
		float c4 = -6.6667f;
		float c5 = -1.1111f;
		System.out.printf("%-12s|%10s\n", "Fahrenheit", "Celsius");
		System.out.printf("-----------------------\n");
		System.out.printf("%+-12d|%+10.2f\n", f1, c1);
		System.out.printf("%+-12d|%+10.2f\n", f2, c2);
		System.out.printf("%+-12d|%+10.2f\n", f3, c3);
		System.out.printf("%+-12d|%+10.2f\n", f4, c4);
		System.out.printf("%+-12d|%+10.2f\n", f5, c5);
	}

}
