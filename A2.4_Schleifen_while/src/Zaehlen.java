import java.util.Scanner;

public class Zaehlen {

	public static void main(String[] args)
	{
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Z�hler");
		System.out.println("Bitte geben Sie eine Zahl ein");
		int number = myScanner.nextInt();
		System.out.println("Bitte geben Sie eine die Z�hlrichtung ein (u f�r aufw�rts, d f�r abw�rts)");
		char direction = myScanner.next().charAt(0);
		int i = 1;
		myScanner.close();
		if(direction == 'u')
		{
			while(i <= number)
			{
				System.out.println(i);
				 i++;
			}
		}
		else if(direction == 'd')
		{
			i = number;
			while(i >= 1)
			{
				System.out.println(i);
				i--;
			}
		}
	}

}