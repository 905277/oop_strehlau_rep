import java.util.Scanner;

public class Zinseszins
{
	public static void main(String[] args)
	{
		Scanner myScanner = new Scanner(System.in);
		System.out.print("Laufzeit (in Jahren) des Sparvertrages: ");
		int runtime = myScanner.nextInt();
		System.out.print("Wie viel Kapital (in Euro) m�chten Sie anlegen: ");
		double startCapital = myScanner.nextInt();
		System.out.print("Zinssatz (in Prozent): ");
		double interest = myScanner.nextDouble();
		interest = interest / 100;
		double endCapital = 0;
		double capital = startCapital;
		while(runtime > 0)
		{
			capital += capital * interest;
			runtime--;
		}
		endCapital = capital;
		System.out.printf("Eingezahltes Kapital: %.2f\n", startCapital);
		
		System.out.printf("Ausgezahltes Kapital: %.2f\n", endCapital);
		myScanner.close();
	}
}