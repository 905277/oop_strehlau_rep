import java.util.Scanner;

public class Quersumme
{
	public static void main(String[] args)
	{
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Quersummenrechner");
		System.out.println("Bitte geben Sie eine Zahl ein");
		int number = myScanner.nextInt();
		int result = 0;
		myScanner.close();
		if(number >= 0)
		{
			while(number > 0)
			{
				result += number;
				number--;
			}
		}
		else
		{
			while(number < 0)
			{
				result += number;
				number++;
			}
		}
		System.out.printf("Die Quersumme ist %d", result);
	}
}