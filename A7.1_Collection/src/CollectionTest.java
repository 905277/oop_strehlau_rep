import java.util.ArrayList;

public class CollectionTest
{
	public static void main(String[] args)
	{
		Buch b1 = new Buch();
		Buch b2 = new Buch("Apfel", "H.P.Lovecraft", 1337);
		Buch b3 = new Buch("Brine", "tfarcevoL.P.H", 1337);
		ArrayList<Buch> col1 = new ArrayList<Buch>();
		col1.add(b1);
		col1.add(b2);
		col1.add(b3);
		System.out.println(getBook(1337, col1));
	}
	
	public static int getBook(int isbn, ArrayList<Buch> collection)
	{
		for(int i = 0; i < collection.size(); i++)
		{
			Object bTemp = collection.get(i);
			Buch bTemp2 = (Buch) bTemp;
			if(bTemp2.getISBN() == isbn)
			{
				return isbn;
			}
		}
		return 0;
	}
}