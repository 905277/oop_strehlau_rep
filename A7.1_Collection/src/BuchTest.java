public class BuchTest
{
	public static void main(String[] args)
	{
		Buch b1 = new Buch();
		Buch b2 = new Buch("Apfel", "H.P.Lovecraft", 1337);
		Buch b3 = new Buch("Brine", "tfarcevoL.P.H", 1337);
		System.out.println(b1.equals(b3));
		System.out.println(b2.equals(b3));
		System.out.println(b1.compareTo(b3));
		System.out.println(b2.compareTo(b3));
	}
}