public class Buch implements Comparable<Buch>
{
	private String autor;
	private String titel;
	private int isbn;
	
	public Buch()
	{
		setAutor("Test");
		setTitel("Test");
		setISBN(1234);
	}
	public Buch(String autor, String titel, int isbn)
	{
		setAutor(autor);
		setTitel(titel);
		setISBN(isbn);
	}
	
	public void setAutor(String autor)
	{
		this.autor = autor;
	}
	
	public void setTitel(String titel)
	{
		this.titel = titel;
	}
	
	public void setISBN(int isbn)
	{
		this.isbn = isbn;
	}
	
	public String getAutor()
	{
		return this.autor;
	}
	
	public String getTitel()
	{
		return this.titel;
	}
	
	public int getISBN()
	{
		return this.isbn;
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if(obj instanceof Buch)
		{
			Buch bTemp = (Buch) obj;
			return this.isbn == bTemp.getISBN();
		}
		else
		{
			return false;
		}	
	}
	
	public int compareTo(Buch o)
	{
		if(this.isbn < o.getISBN())
		{
			return -1;
		}
		if(this.isbn > o.getISBN())
		{
			return 1;
		}
		return 0;
	}
}