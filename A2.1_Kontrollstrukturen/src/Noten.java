
import java.util.Scanner;
public class Noten {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Bitte geben Sie die Note ein");
		int mark = myScanner.nextInt();
		myScanner.close();
		switch (mark) {
		case 1:
			System.out.println("Sehr gut");
			break;
		case 2:
			System.out.println("Gut");
			break;
		case 3:
			System.out.println("Befriedigend");
			break;
		case 4:
			System.out.println("Ausreichend");
			break;
		case 5:
			System.out.println("Mangelhast");
			break;
		case 6:
			System.out.println("Ungenügend");
			break;
		default:
			System.out.println("Dies ist keine korrekte Notenangabe");
			break;
		}
	}

}
