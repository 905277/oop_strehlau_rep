
import java.util.Scanner;
public class BMIRechner {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println("BMI Rechner");
		System.out.println("Bitte geben Sie Ihr Körpergewicht in kg an");
		float weight = myScanner.nextFloat();
		System.out.println("Bitte geben Sie Ihre Körpergröße in (ganzzahligen) cm an");
		float height = myScanner.nextFloat();
		System.out.println("Bitte geben Sie Ihr Geschlecht an (m/w)");
		char gender = myScanner.next().charAt(0);
		while (gender != 'm' && gender != 'w') {
			System.out.println("Sie keins der beiden zur Auswahl stehenden Geschlechter angegeben, bitte versuchen Sie es erneut");
			System.out.println("Bitte geben Sie Ihr Geschlecht an (m/w)");
			gender = myScanner.next().charAt(0);
		}
		myScanner.close();
		height = height / 100;
		float bmi = weight / height / height;
		if (gender == 'm') {
			if (bmi < 20) {
				System.out.printf("Bei einem BMI von %.1f bei einem Mann handelt es sich um Untergewicht", bmi);
			}
			else if (bmi <= 25) {
				System.out.printf("Bei einem BMI von %.1f bei einem Mann handelt es sich um Normalgewicht", bmi);
			}
			else {
				System.out.printf("Bei einem BMI von %.1f bei einem Mann handelt es sich um Übergewicht", bmi);
			}
		}
		else if (gender == 'w') {
			if (bmi < 19) {
				System.out.printf("Bei einem BMI von %.1f bei einer Frau handelt es sich um Untergewicht", bmi);
			}
			else if (bmi <= 24) {
				System.out.printf("Bei einem BMI von %.1f bei einer Frau handelt es sich um Normalgewicht", bmi);
			}
			else {
				System.out.printf("Bei einem BMI von %.1f bei einer Frau handelt es sich um Übergewicht", bmi);
			}
		}
		else {
			System.out.println("Fehler");
		}
	}
	
}
