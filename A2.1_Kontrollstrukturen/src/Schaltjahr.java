
import java.util.Scanner;
public class Schaltjahr {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Schaltjahrrechner");
		System.out.println("Bitte geben Sie die Jahreszahl ein");
		float year = myScanner.nextFloat();
		myScanner.close();
		float yearRule1 = year / 4;
		float yearRule2 = year / 100;
		float yearRule3 = year / 400;
		if (yearRule1 % 1 == 0 && yearRule2 % 1 != 0) {
			System.out.println("Es handelt sich um ein Schaltjahr");
		}
		else if(yearRule1 % 1 == 0 && yearRule2 % 1 == 0 && yearRule3 % 1 == 0) {
			System.out.println("Es handelt sich um ein Schaltjahr");
		}
		else {
			System.out.println("Es handelt sich um KEIN Schaltjahr");
		}
	}
	
}
