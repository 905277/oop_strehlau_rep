public class DatumTest
{
	public static void main(String[] args) throws TagException, MonatException, JahrException
	{
		try
		{
			Datum d1 = new Datum();
			Datum d2 = new Datum(111,2,2000);
			System.out.println(d1.toString());
			System.out.println(d2.toString());
		}
		catch(TagException ex)
		{
			System.out.println(ex.getMessage());
			System.out.println(ex.getWert());
		}
		catch(MonatException ex)
		{
			System.out.println(ex.getMessage());
			System.out.println(ex.getWert());
		}
		catch(JahrException ex)
		{
			System.out.println(ex.getMessage());
			System.out.println(ex.getWert());
		}
	}
}