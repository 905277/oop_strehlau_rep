public class Datum
{
	private int tag;
	private int monat;
	private int jahr;
	
	public Datum() throws TagException, MonatException, JahrException
	{
		setTag(1);
		setMonat(1);
		setJahr(1970);
	}
	public Datum(int tag, int monat, int jahr) throws TagException, MonatException, JahrException
	{
		setTag(tag);
		setMonat(monat);
		setJahr(jahr);
	}
	public void setTag(int tag) throws TagException
	{
		if(tag > 0 && tag < 32)
			this.tag = tag;
		else
			throw new TagException("Der Wert des Parameters \"Tag\" ist au�erhalb der Grenzen", tag);
	}
	public void setMonat(int monat) throws MonatException
	{
		if(monat > 0 && monat < 13)
			this.monat = monat;
		else
			throw new MonatException("Der Wert des Parameters \"Monat\" ist au�erhalb der Grenzen", monat);
	}
	public void setJahr(int jahr) throws JahrException
	{
		if(jahr > 1899 && jahr < 2101)
			this.jahr = jahr;
		else
			throw new JahrException("Der Wert des Parameters \"Jahr\" ist au�erhalb der Grenzen", jahr);
	}
	public int getTag()
	{
		return this.tag;
	}
	public int getMonat()
	{
		return this.monat;
	}
	public int getJahr()
	{
		return this.jahr;
	}
	
	@Override
	public String toString()
	{
		return tag + "." + monat + "." + jahr;
	}
	public boolean equals(Object obj)
	{
		if(obj instanceof Datum)
		{
			Datum dTemp = (Datum) obj;
			return this.tag == dTemp.getTag() && this.monat == dTemp.getMonat() && this.jahr == dTemp.getJahr();
		}
		else
		{
			return false;
		}	
	}
}
