public class Division
{
	public static void main(String[] args)
	{
		System.out.println(division(3, 2));
	}
	
	static int division(int zahl1, int zahl2)
	{
		try
		{
		int ergebnis = zahl1/zahl2;
		return ergebnis;
		}
		catch(ArithmeticException ex)
		{
		System.out.println("Division durch Null");
		return 0;
		}
	}
}