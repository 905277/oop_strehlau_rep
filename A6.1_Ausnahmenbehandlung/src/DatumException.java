public class DatumException extends Exception
{
	private int wert;
	
	public DatumException(String msg, int wert)
	{
		super(msg);	
		this.wert = wert;
	}
	
	public int getWert()
	{
		return wert;
	}
}