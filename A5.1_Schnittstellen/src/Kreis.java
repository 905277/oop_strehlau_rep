public class Kreis extends GeoObject
{
	private double radius;
	
	public Kreis(double x, double y, double radius)
	{
		super(x,y);
		this.radius = radius;
	}
	
	public double determineArea()
	{
		return this.radius * this.radius * Math.PI;
	}
	
	@Override
	public String toString()
	{
		return "Rechteck [x=" + getX() + ", y=" + getY() + ", radius=" + this.radius + "]";
	}
}