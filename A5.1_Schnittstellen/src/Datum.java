public class Datum implements Comparable<Datum>
{
	private int tag;
	private int monat;
	private int jahr;
	
	public Datum()
	{
		setTag(1);
		setMonat(1);
		setJahr(1970);
	}
	public Datum(int tag, int monat, int jahr)
	{
		setTag(tag);
		setMonat(monat);
		setJahr(jahr);
	}
	public void setTag(int tag)
	{
		this.tag = tag;
	}
	public void setMonat(int monat)
	{
		this.monat = monat;
	}
	public void setJahr(int jahr)
	{
		this.jahr = jahr;
	}
	public int getTag()
	{
		return this.tag;
	}
	public int getMonat()
	{
		return this.monat;
	}
	public int getJahr()
	{
		return this.jahr;
	}
	public static int quarter(Datum dTemp)
	{
		int month = dTemp.getMonat(); 
		int quarter = 0;
		if(month > 0 && month < 4)
		{
			quarter = 1;
		}
		else if(month > 3 && month < 7)
		{
			quarter = 2;
		}
		else if(month > 6 && month < 10)
		{
			quarter = 3;
		}
		else if(month > 9 && month < 13)
		{
			quarter = 4;
		}
		return quarter;
	}
	
	@Override
	public String toString()
	{
		return tag + "." + monat + "." + jahr;
	}
	public boolean equals(Object obj)
	{
		if(obj instanceof Datum)
		{
			Datum dTemp = (Datum) obj;
			return this.tag == dTemp.getTag() && this.monat == dTemp.getMonat() && this.jahr == dTemp.getJahr();
		}
		else
		{
			return false;
		}	
	}
	
	public int compareTo(Datum o)
	{
		if(this.jahr < o.getJahr())
		{
			return -1;
		}
		else if(this.jahr > o.getJahr())
		{
			return 1;
		}
		else if(this.monat < o.getMonat())
		{
			return -1;
		}
		else if(this.monat > o.getMonat())
		{
			return 1;
		}
		else if(this.tag < o.getTag())
		{
			return -1;
		}
		else if(this.tag > o.getTag())
		{
			return 1;
		}
		return 0;
	}
}
