public class Rechteck extends GeoObject
{
	private double height;
	private double width;
	
	public Rechteck(double x, double y, double height, double width)
	{
		super(x,y);
		this.height = height;
		this.width = width;
	}
	
	public double determineArea()
	{
		return this.height * this.width;
	}
	
	@Override
	public String toString()
	{
		return "Rechteck [x=" + getX() + ", y=" + getY() + ", height=" + this.height + ", width=" + this.width + "]";
	}
}