public class Komplex
{
	double real;
	double imag;
	public Komplex()
	{
		setReal(0.0);
		setImag(0.0);
	}
	public Komplex(double real, double imag)
	{
		setReal(real);
		setImag(imag);
	}
	public void setReal(double real)
	{
		this.real = real;
	}
	public void setImag(double imag)
	{
		this.imag = imag;
	}
	public double getReal()
	{
		return this.real;
	}
	public double getImag()
	{
		return this.imag;
	}
	
	public Komplex multiply(Komplex k)
	{
		double resultReal = this.real * k.getReal() - this.imag * k.getImag();
		double resultImag = this.real * k.getImag() - k.getReal() * this.imag;
		Komplex result = new Komplex(resultReal, resultImag);
		return result;
	}
	
	public static Komplex multiply(Komplex k1, Komplex k2)
	{
		double resultReal = k1.getReal() * k2.getReal() - k1.getImag() * k2.getImag();
		double resultImag = k1.getReal() * k2.getImag() - k2.getReal() * k1.getImag();
		Komplex result = new Komplex(resultReal, resultImag);
		return result;
	}
	
	@Override
	public String toString()
	{
		return "Real: " + real + " Imagin�r: " + imag;
	}
	public boolean equals(Object obj)
	{
		if(obj instanceof Komplex)
		{
			Komplex kTemp = (Komplex) obj;
			return this.real == kTemp.getReal() && this.imag == kTemp.getImag();
		}
		else
		{
			return false;
		}	
	}
	
}