public class PersonTest {
	public static void main(String[] args)
	{
		Person p1 = new Person();
		Person p2 = new Person("Maximilian", new Datum(1, 2, 3000));
		System.out.println(p1);
		System.out.println(p2);
		System.out.println(p1.getPersonennummer());
		System.out.println(p2.getPersonennummer());
	}
}