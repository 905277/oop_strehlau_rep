public class Person
{
	private String name;
	private Datum geburtsdatum;
	private int personennummer;
	private static int z�hler = 1;
	
	public Person()
	{
		setName("Mustermann");
		setGeburtsdatum(new Datum(1, 1, 1900));
		setPersonennummer(z�hler);
		z�hler++;
	}
	public Person(String name, Datum datum)
	{
		setName(name);
		setGeburtsdatum(datum);
		setPersonennummer(z�hler);
		z�hler++;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	public String getName()
	{
		return this.name;
	}
	public void setGeburtsdatum(Datum datum)
	{
		this.geburtsdatum = datum;
	}
	public String getGeburtsdatum()
	{
		return geburtsdatum.getTag() + "." + geburtsdatum.getMonat() + "." + geburtsdatum.getJahr();
	}
	public void setPersonennummer(int nummer)
	{
		this.personennummer= nummer;
	}
	public int getPersonennummer()
	{
		return this.personennummer;
	}
	
	@Override
	public String toString()
	{
		return "Name: " + this.name + "\nGeburtsdatum: " + this.geburtsdatum;
	}
}