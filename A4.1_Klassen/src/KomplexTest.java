public class KomplexTest
{
	public static void main(String[] args)
	{
		Komplex k1 = new Komplex(1.0, 2.0);
		Komplex k2 = new Komplex(2.0, 1.0);
		System.out.println(k1);
		System.out.println(Komplex.multiply(k1, k2));
		System.out.println(k1.multiply(k2));
	}
}