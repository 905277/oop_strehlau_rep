import java.util.Scanner;

public class PCHaendler {

	public static void main(String[] args)
	{
		String artikel = liesString("Was m�chten Sie bestellen?");
		int anzahl = liesInt("Geben Sie die Anzahl ein:");
		double preis = liesDouble("Geben Sie den Nettopreis ein:");
		double mwst = liesDouble("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		double nettogesamtpreis = berechneGesamtnettopreis(anzahl, preis);
		double bruttogesamtpreis = berechneGesamtbruttopreis(nettogesamtpreis, mwst);
		rechungausgeben(artikel, anzahl, nettogesamtpreis, bruttogesamtpreis, mwst);
	}
	public static String liesString(String text)
	{
		Scanner artikelScanner = new Scanner(System.in);
		System.out.println(text);
		String artikel = artikelScanner.next();
		return artikel;
	}
	public static int liesInt(String text)
	{
		Scanner anzahlScanner = new Scanner(System.in);
		System.out.println(text);
		int anzahl = anzahlScanner.nextInt();
		return anzahl;
	}
	public static double liesDouble(String text)
	{
		Scanner preisScanner = new Scanner(System.in);
		System.out.println(text);
		double preis = preisScanner.nextDouble();
		return preis;
	}
	public static double berechneGesamtnettopreis(int anzahl, double nettopreis)
	{
		double nettogesamtpreis = anzahl * nettopreis;
		return nettogesamtpreis;
	}
	public static double berechneGesamtbruttopreis(double nettogesamtpreis, double mwst)
	{
		double bruttogesamtpreis = nettogesamtpreis * (1 + mwst / 100);
		return bruttogesamtpreis;
	}
	public static void rechungausgeben(String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis, double mwst)
	{
		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
	}
}