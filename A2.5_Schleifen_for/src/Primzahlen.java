public class Primzahlen
{
	public static void main(String[] args)
	{
		for(int i = 2; i <= 100; i++)
		{
			boolean notPrime = false;
			for(int j = 2; j <= i / 2; ++j)
			{
				if(i % j == 0)
				{
					notPrime = true;
					break;
				}
			}
			if(!notPrime)
			{
				System.out.printf("%3d", i);
			}
		}
	}
}
