import java.util.Scanner;

public class Zaehlen
{
	public static void main(String[] args)
	{
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Z�hler");
		System.out.println("Bitte geben Sie eine Zahl ein");
		int number = myScanner.nextInt();
		System.out.println("Bitte geben Sie eine die Z�hlrichtung ein (u f�r aufw�rts, d f�r abw�rts)");
		char direction = myScanner.next().charAt(0);
		myScanner.close();
		if(direction == 'u')
		{
			for(int i = 1; i <= number; i++)
			{
				System.out.println(i);
			}
		}
		else if(direction == 'd')
		{
			for(int i = number; i > 0; i--)
			{
				System.out.println(i);
			}
		}
	}
}