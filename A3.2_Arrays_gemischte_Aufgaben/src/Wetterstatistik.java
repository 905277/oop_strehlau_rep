
import java.util.Scanner;
public class Wetterstatistik
{
	public static void main(String[] args)
	{
		double[] values = {0,1,2,3,4,5,6,7,8,9,10};
		char choice = menu();
		output(choice, values);
		
	}
	public static char menu()
	{
		Scanner myScanner = new Scanner(System.in);
		System.out.println("+----------------------+");
		System.out.println("| (M)ittelwert         |");
		System.out.println("|  M(i)nimum           |");
		System.out.println("|  Ma(x)imum           |");
		System.out.println("+----------------------+");
		System.out.println("|  Be(e)nden           |");
		System.out.println("+----------------------+");
		char input = myScanner.next().charAt(0);
		input = Character.toLowerCase(input);
		while(input != 'm' && input != 'i' && input != 'x' && input != 'e')
		{
			System.out.println("Inkorrekte Auswahl, bitte versuchen Sie es erneut");
			input = myScanner.next().charAt(0);
			input = Character.toLowerCase(input);
		}
		myScanner.close();
		return input;
	}
	public static void output(char outputChoice, double[] outputValues)
	{
		double outputTemperature;
		switch(outputChoice)
		{
		case 'm':
			outputTemperature = 0;
			for(int i = 0; i < outputValues.length; i++)
			{
				outputTemperature += outputValues[i];
			}
			outputTemperature = outputTemperature / outputValues.length;
			break;
		case 'i':
			outputTemperature = 100;
			for(int i = 0; i < outputValues.length; i++)
			{
				if (outputValues[i] < outputTemperature)
					outputTemperature = outputValues[i];
			}
			break;
		case 'x':
			outputTemperature = -100;
			for(int i = 0; i < outputValues.length; i++)
			{
				if (outputValues[i] > outputTemperature)
					outputTemperature = outputValues[i];
			}
			break;
		case 'e':
			outputTemperature = 9999;
			break;
		default:
			outputTemperature = 9999;
			break;
		}
		if (outputTemperature == 9999)
		{
			System.out.println("Programm beendet");
		}
		else if (outputChoice == 'm')
		{
			System.out.println("Der Mittelwert betr�gt " + outputTemperature + " Grad Celsius");
		}
		else if (outputChoice == 'i')
		{
			System.out.println("Der kleinste Wert betr�gt " + outputTemperature + " Grad Celsius");
		}
		else if (outputChoice == 'x')
		{
			System.out.println("Der gr��te Wert betr�gt " + outputTemperature + " Grad Celsius");
		}
	}
}
