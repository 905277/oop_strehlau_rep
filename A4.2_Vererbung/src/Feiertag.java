public class Feiertag extends Datum
{
	private String name;
	
	public Feiertag()
	{
		super(1, 1, 1900);
		setName("Platzhalter");
	}
	
	public Feiertag(String name, int tag, int monat, int jahr)
	{
		super(tag, monat, jahr);
		setName(name);
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	public String getName()
	{
		return this.name;
	}

	@Override
	public String toString()
	{	
		return this.getTag() + "." + this.getMonat() + "." + this.getJahr() + " (" + this.name + ")";
	}
}