public class Bankkonto
{
	private String inhaber;
	private int kontonummer;
	private double kontostand;
	
	public Bankkonto()
	{
		setInhaber("Mustermann");
		setKontonummer(123456);
		setKontostand(0);
	}
	
	public Bankkonto(String inhaber, int kontonummer, double kontostand)
	{
		setInhaber(inhaber);
		setKontonummer(kontonummer);
		setKontostand(kontostand);
	}
	
	public void setInhaber(String inhaber)
	{
		this.inhaber = inhaber;
	}
	public String getInhaber()
	{
		return this.inhaber;
	}
	public void setKontonummer(int kontonummer)
	{
		this.kontonummer = kontonummer;
	}
	public int getKontonummer()
	{
		return this.kontonummer;
	}
	public void setKontostand(double kontostand)
	{
		this.kontostand = kontostand;
	}
	public double getKontostand()
	{
		return this.kontostand;
	}
	
	public String einzahlen(double betrag)
	{
		this.kontostand = this.kontostand + betrag ;
		return "Neuer Kontostand: " + this.kontostand;
	}
	public String auszahlen(double betrag)
	{
		this.kontostand = this.kontostand - betrag;
		return betrag + " abgehoben. Neuer Kontostand: " + this.kontostand;
	}
	
	@Override
	public String toString()
	{
		return "Inhaber: " + this.inhaber + " Kontonummer: " + this.kontonummer + " Kontostand: " + this.kontostand;
	}
}