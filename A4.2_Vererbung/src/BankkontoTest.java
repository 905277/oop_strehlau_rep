public class BankkontoTest
{
	public static void main(String[] args)
	{
		Bankkonto bk1 = new Bankkonto();
		Bankkonto bk2 = new Bankkonto("Strehlau", 654321, 1337.69);
		Bankkonto bk3 = new Bankkonto("Schnitt", 13579, 100);
		Dispokonto bk4 = new Dispokonto("Schnitt", 24680, 0, 100);
		System.out.println(bk1);
		System.out.println(bk2);
		System.out.println(bk1.auszahlen(200));
		System.out.println(bk2.einzahlen(662.31));
		System.out.println(bk4.einzahlen(100));
		System.out.println(bk3.auszahlen(300));
		System.out.println(bk4.auszahlen(300));
	}
}