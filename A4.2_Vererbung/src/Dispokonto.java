public class Dispokonto extends Bankkonto
{
	private double dispokredit;
	
	public Dispokonto()
	{
		super();
		setDispokredit(100);
	}
	public Dispokonto(String inhaber, int kontonummer, double kontostand, double dispokredit)
	{
		super(inhaber, kontonummer, kontostand);
		setDispokredit(dispokredit);
	}
	
	public void setDispokredit(double dispokredit)
	{
		this.dispokredit = dispokredit;
	}
	public double getDispokredit()
	{
		return this.dispokredit;
	}
	
	public String auszahlen(double betrag)
	{
		if((this.getKontostand() - betrag) >= 0 - this.dispokredit)
		{
			this.setKontostand(this.getKontostand() - betrag);
			return betrag + " abgehoben. Neuer Kontostand: " + this.getKontostand();
		}
		else /*if((this.getKontostand() - betrag) < 0 - this.dispokredit)*/
		{
			betrag = this.getKontostand() + this.dispokredit;
			this.setKontostand(0 - dispokredit);
			return "Dispokredit überschritten, stattdessen " + betrag + " abgehoben. Neuer Kontostand: " + this.getKontostand();
		}
		
	}
	
	@Override
	public String toString()
	{
		return "Inhaber: " + this.getInhaber() + " Kontonummer: " + this.getKontonummer() + " Kontostand: " + this.getKontostand() + "Dispokredit: " + this.dispokredit;
	}
}