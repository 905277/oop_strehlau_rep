
public class Lotto
{
	public static void main(String[] args)
	{
		int[] lottozahlen = {3, 7, 12, 18, 37, 42};
		System.out.printf("[ ");
		for(int i = 0; i < lottozahlen.length; i++)
		{
			System.out.printf(" %d ", lottozahlen[i]);
		}
		System.out.printf(" ]\n");
		boolean is12there = false;
		boolean is13there = false;
		for(int i = 0; i < lottozahlen.length && is12there == false && is13there == false; i++)
		{
			if(lottozahlen[i] == 12)
			{
				System.out.println("Die Zahl 12 ist in der Ziehung enthalten.");
				is12there = true;
			}
			if(lottozahlen[i] == 13)
			{
				System.out.println("Die Zahl 13 ist in der Ziehung enthalten.");
				is13there = true;
			}
		}
		if(is12there == false)
		{
			System.out.println("Die Zahl 12 ist nicht in der Ziehung enthalten.");
		}
		if(is13there == false)
		{
			System.out.println("Die Zahl 13 ist nicht in der Ziehung enthalten.");
		}
	}
}
