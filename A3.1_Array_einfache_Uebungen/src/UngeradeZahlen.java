
public class UngeradeZahlen
{
	public static void main(String[] args)
	{
		int[] zahlen = new int[10];
		for(int i = 1; i < 20; i += 2)
		{
			zahlen[(i-1)/2] = i;
		}
		for(int j = 0; j < 10; j++)
		{
			System.out.printf("%-3d", zahlen[j]);
		}
		
		System.out.println();
		
		int[] zahlen2 = new int[10];
		for(int k = 0; k < 10; k++)
		{
			zahlen2[k] = k*2+1;
		}
		for(int j = 0; j < 10; j++)
		{
			System.out.printf("%-3d", zahlen2[j]);
		}
	}
}
